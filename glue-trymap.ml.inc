let trymap = fun v -> fun () -> try Ok (v ()) with e -> Err (string2coqstring (Printexc.to_string e))

let trymap_stderr = fun v -> fun () ->
  let r = (trymap v) ()
  in match r with
  | Err m -> prerr_string (coqstring2string m); prerr_string "\n"; r
  | Ok _ -> (); r
