Module PRNGActions
.

Inductive prng_action :=
| prng_action__getbit : prng_action.

Definition eq_prng_action_dec :
  forall a1 a2 : prng_action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  decide equality.
Defined.

Definition prng_action_res
           (a : prng_action) :
  Set :=
  match a with
  | prng_action__getbit => bool
  end.

Definition eq_prng_action_res_dec :
  forall (a : prng_action) (res1 res2 : prng_action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  destruct a; simpl in *; decide equality.
Defined.

End PRNGActions.
