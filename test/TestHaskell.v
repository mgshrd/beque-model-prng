Require Import Beque.Util.ltac_lens.
Require Import Beque.Util.ltac_utils.
Require Beque.Util.common_extract.
Require Beque.Util.DelayExtractionDefinitionToken.

Require Import Beque.IO.impl.AxiomaticIOInAUnitWorld.
Require Import Beque.IO.intf.IOAux.
Require Import Beque.IO.util.IODebug.
Require Import Beque.IO.util.ResultUtils.

Require Import PRNGImpl.

Require Import Beque.Model.Log.impl.LogImpl.

Extraction Language Haskell.

Module comext.
Include common_extract.commonExtractHaskell DelayExtractionDefinitionToken.token.
End comext.

Module ioext.
Include AxiomaticIOInAUnitWorld.extractHaskell DelayExtractionDefinitionToken.token.
End ioext.

Module iod := IODebug UnitWorld AxiomaticIOInAUnitWorld.
Import iod.
Module ru := ResultUtils UnitWorld AxiomaticIOInAUnitWorld.
Import ru.

Module prng.
Include PRNGImpl AxiomaticIOInAUnitWorld.UnitWorld AxiomaticIOInAUnitWorld.
Include prng.extractHaskell DelayExtractionDefinitionToken.token.
End prng.

Module log.
Include LogImpl AxiomaticIOInAUnitWorld.UnitWorld AxiomaticIOInAUnitWorld.
End log.
Module loge := log.extractHaskell DelayExtractionDefinitionToken.token.

Definition warn := log.log log_level.log_level_warning.
Definition info := log.log log_level.log_level_info.
Open Scope string_scope.
Definition bool2string (b : bool) := if b then "true" else "false".
Coercion bool2string : bool >-> String.string.
Close Scope string_scope.
Program Fixpoint reduce mm n :=
  match n with
  | O => |\? "fixpoint" \ IO_return mm
  | S n' => do? "recurse" \
              mm' <--? "reduce" \ reduce mm n';;
              b <--? "getbit" \ prng.getbit;;
              res <-- IO_return (andb (fst mm') b, orb (snd mm') b);;
              (*_ <-- info (fst mm');;
              _ <-- info (snd mm');;
              _ <-- info b;;*)
              /| IO_return res
  end.
Next Obligation. exact (mark "common postcond"). Defined.
Next Obligation. simpl. constructor. Defined.
Next Obligation. let tl := takelast s in exact (tl s). Defined.
Next Obligation. desig. simpl in *. subst. simpl. constructor. Defined.

Program Definition main :=
  min_max <== reduce (true, false) 1000;;
    _ <-- info (String.append "min: " (fst min_max));;
    _ <-- info (String.append "max: " (snd min_max));;
    _ <?-- IO_return (if fst min_max
                      then result.err tt
                      else result.ok tt)
      [err /| log.log log_level.log_level_warning "unexpected minimum"];;
    _ <?-- IO_return (if snd min_max
                      then result.ok tt
                      else result.err tt)
      [err /| log.log log_level.log_level_warning "unexpected maximum" ];;
    /| IO_return tt.

Extraction "test" main.
